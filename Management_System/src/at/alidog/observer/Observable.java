package at.alidog.observer;

public interface Observable {
public String inform();
}
