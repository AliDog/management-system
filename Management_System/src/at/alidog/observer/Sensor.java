package at.alidog.observer;

import java.util.ArrayList;
import java.util.List;
import at.alidog.observer.Observable;
import at.alidog.observer.Observer;

public class Sensor implements Observer{
	
	private List<Observable> observables = new ArrayList<Observable>();

	@Override
	public void addItem(Observable observable) {
		this.observables.add(observable);
		
	}

	@Override
	public void informAll() {
		for(Observable o : this.observables) {
			System.out.println(o.inform());
		}
		
	}

}
