package at.alidog.observer;

public interface Observer {

public void addItem(Observable observable);
public void informAll();

	
}
