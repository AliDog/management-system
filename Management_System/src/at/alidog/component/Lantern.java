package at.alidog.component;

import at.alidog.component.Component;
import at.alidog.observer.Observable;

public class Lantern implements Component, Observable {

	private boolean isAlive = false;
	
	@Override
	public void start() {
		isAlive = true;
		
	}

	@Override
	public boolean isalive() {
		return this.isalive();
		
	}

	@Override
	public String inform() {
		
		return "Informing Lantern";
	}

}
