package at.alidog.component;

import java.util.ArrayList;
import java.util.List;

public class Management_System {

	private String name;
	private List<Component> components = new ArrayList<Component>();
	
	public Management_System(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void addComponent(Component e) {
		this.components.add(e);
	}
}
