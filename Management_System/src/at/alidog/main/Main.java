package at.alidog.main;



import at.alidog.component.Christmas;
import at.alidog.component.Component;
import at.alidog.component.Lantern;
import at.alidog.component.Management_System;
import at.alidog.component.Traffic_Light;
import at.alidog.observer.Observable;
import at.alidog.observer.Sensor;

public class Main {

	public static void main(String[] args) {
		
		Management_System management = new Management_System("System 1");
		
		Component component1 = new Lantern();
		Component component2 = new Traffic_Light();
		Component component3 = new Christmas();
		
		management.addComponent(component1);
		management.addComponent(component2);
		management.addComponent(component3);
		
		
		Sensor sensor = new Sensor();
		
		sensor.addItem((Observable) component1);
		sensor.addItem((Observable) component2);
		sensor.addItem((Observable) component3);
		
		sensor.informAll();
	}

}